
#include <iostream>
#include <ostream>
#include <vector>

class Engine
{
public:
	Engine(float PW) :power(PW)
	{
		//std::cout << "Engine: " << power;
	}

	float power;

	float getValuePower() const { return power; }
	//virtual const Engine* getEngine() const = 120;

	virtual std::ostream& print(std::ostream& out) const
	{
		out << "Engine: " << power;
		return out;
	}

	//friend std::ostream& operator<<(std::ostream& os, const Engine& obj)
	friend std::ostream& operator<<(std::ostream& os, const Engine& obj)
	{
		return obj.print(os);
	}
};

class Vehicle
{
public:
	Vehicle() {}

	virtual std::ostream& print(std::ostream& out) const = 0;

	friend std::ostream& operator<<(std::ostream& os, const Vehicle& obj)
	{
		return obj.print(os);
	}

	//float getValuePower() { return 0; }
	virtual const Engine* getEngine() const = 0;
};


class WaterVehicle : public Vehicle
{
public:
	WaterVehicle(float ValueBC) : BoatDraft(ValueBC) {}

	float BoatDraft;
	float GetValuePower() const { return 0; }

	std::ostream& print(std::ostream& out) const override
		//virtual std::ostream& print(std::ostream& out)
	{
		out << "Boat draft: " << BoatDraft;
		return out;
	}

	const Engine* getEngine() const override
	{
		return 0;
	}
};

class RaodVehicle : public Vehicle
{
protected:
	float GroundCleaner;

public:
	RaodVehicle(float GC):GroundCleaner(GC) {}

	std::ostream& print(std::ostream& out) const override
	//virtual std::ostream& print(std::ostream& out)
	{
		out << ", Ride height: " << GroundCleaner;
		return out;
	}
};

class Wheel
{
protected:
	float diameter;

public:
	Wheel(float dim):diameter(dim)
	{
		//std::cout << ", Wheel: " << diameter;
	}

	float GetDiametrWheel() const
	{
		return diameter;
	}

};



class Bicycle : public RaodVehicle
{
protected:
	Wheel dWheel1, dWheel2;

public:
	Bicycle(Wheel diametr1, Wheel diametr2, float GroundCleaner) : RaodVehicle(GroundCleaner),
		dWheel1(diametr1), dWheel2(diametr2)
		{}

	std::ostream& print(std::ostream& out) const override
	{
		out << "Bicycle, "
			<< ", Wheel: " << dWheel1.GetDiametrWheel()
			<< ", " << dWheel2.GetDiametrWheel();
		RaodVehicle::print(out);
		return out;
	}

	const Engine* getEngine() const override
	{
		return 0;
	}
};

class Car : public RaodVehicle
{
protected:
	Engine Motor;
	Wheel dWheel1, dWheel2, dWheel3, dWheel4;
	//RaodVehicle GCleaner;

public:
	Car(Engine power, Wheel diametr1, Wheel diametr2, Wheel diametr3, Wheel diametr4, float
		GroundCleaner) : RaodVehicle(GroundCleaner),
	    Motor(power), dWheel1(diametr1), dWheel2(diametr2), dWheel3(diametr3), dWheel4(diametr4)
	{}

	std::ostream& print(std::ostream& out) const override
	{
		out << "Car, " << Motor
			<< ", Wheel: " << dWheel1.GetDiametrWheel()
			<< ", " << dWheel2.GetDiametrWheel()
			<< ", " << dWheel3.GetDiametrWheel()
			<< ", " << dWheel4.GetDiametrWheel();
			//<< ", " << GCleaner;
		RaodVehicle::print(out);
		return out;
	}

	//const Engine* GetValuePower() const { return &Motor; }
	const Engine* getEngine() const override
	{
		return &Motor;
	}
};

float getHighestPower(std::vector<Vehicle*>& v)
{
	float maxPower = 0.f;

	for (int i = 0; i < v.size(); ++i)
	{
		const Engine* engine = v[i]->getEngine();
		if (engine)
		{
			maxPower = std::fmaxf(engine->getValuePower(), maxPower);
		}
		//maxPower = std::fmaxf(v[i]->getValuePower(), maxPower);
 		//maxPower = std::max(0,1);
	}
	
	return maxPower;
}


//std::ostream operator<<(const std::ostream& lhs, const Car& car);

int main()

{
	//part 1
	std::cout << "part 1" << '\n';
	
	Car c(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 150);
	std::cout << c << '\n';
	
	Bicycle t(Wheel(20), Wheel(20), 300);
	std::cout << t << '\n';



	//part 2
	std::cout << "part 2" << '\n';
	std::vector<Vehicle*> v;

	v.push_back(new Car(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 250));
	//v.push_back(new Circle(Point(1, 2, 3), 7));
	v.push_back(new Car(Engine(200), Wheel(19), Wheel(19), Wheel(19), Wheel(19), 130));
	v.push_back(new WaterVehicle(5000));

	//TODO: ����� ��������� ������� v �����
	for (int i = 0; i < v.size(); ++i)
	{
		std::cout << *v[i] << '\n';
	}
	
	std::cout << "The highest power is: " << getHighestPower(v) << '\n';

	//TODO: �������� ��������� ������� v �����
	for (int i = 0; i < v.size(); ++i)
	{
		delete v[i];
	}
	v.clear();

	return 0;

}

//result
//Car Engine : 150 Wheels : 17 17 18 18 Ride height : 150
//Bicycle Wheels : 20 20 Ride height : 300